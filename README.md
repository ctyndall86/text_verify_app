## Example application with mobile phone PIN verification

#### Instructions
1. Clone Text_Verify_App
2. Navigate to the folder and load it with docker using:
	docker load -i text_verify_app.tar
3. Run the image using:
	docker run -it -p 5000:5000 test-verify-app:latest
3. Once the image is loaded, edit the config.py file using vim:
	vim config.py
4. Fill in your twilio account, token, and phone number.
5. Run app.py with python 2.7 using:
        python app.py
6. Point your browser to "0.0.0.0:5000".
NOTE: If you are using DockerQuicktartTerminal/boot2docker, 
	  you may need to point to the IP listed upon loading the QuickStart terminal
	  instead of 0.0.0.0